﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using Countdown.DataAccess;
using Countdown.DataAccess.Entities;

namespace Countdown.BusinessLogic
{
    public class GameController
    {
        private const int MaxNoOfRounds = 4;
        private readonly List<char> vowels = new List<char> { 'a', 'e', 'i', 'o', 'u' };


        public GameModel CurrentGame { get; set; }
        public int CurrentGameRound { get; set; }


        public void CreateNewGame()
        {
            var newGameModel = MoqDbAccess.Instance.CreateNewGame();
            newGameModel.VowelList = new List<char>();
            newGameModel.ConsonantList = new List<char>();
            newGameModel.Score = 0;

            CurrentGameRound++;
            CurrentGame = newGameModel;
        }

        public void EndGame()
        {
            // Verify game is complete.

            if (CurrentGame.IsAllowedToEndGame() == false)
                return;
            
            var bestPossibleScore = LongestWordCalculator.GetLongestWordLengthFromPublicRestApi(CurrentGame.VowelList, CurrentGame.ConsonantList);
            var actualScore = CurrentGame.LongestWordGuess.Value;
            CurrentGame.EndGame();

            MoqDbAccess.Instance.EndGame(CurrentGame, bestPossibleScore, actualScore);

        }

        public char AddVowelToCurrentGame()
        {
            var letterPos = new Random().Next(0, 4);
            var charToReturn = vowels[letterPos];

            CurrentGame.VowelList.Add(charToReturn);
            return charToReturn;
        }
        
        public bool IsGameComplete()
        {
            if (CurrentGameRound >= MaxNoOfRounds)
            {
                return true;
            }
            return false;
        }


        public char AddConsonantToCurrentGame()
        {
            var toReturn = 'a';

            var startChar = (int)'a';
            var endChar = (int)'z';

            do
            {
                toReturn = (char)(new Random().Next(startChar, endChar));
                
            } while (vowels.Contains(toReturn)); // Slight risk of getting stuck in long loop. Normally include a max timer here too.
            
            CurrentGame.ConsonantList.Add(toReturn);

            return toReturn;
        }




        #region Helpers for displaying text to UI

        public string GetBestPossibleScoreFromAllGamesPlayed()
        {
            return MoqDbAccess.Instance.CalculateTotalBestPossibleScore().ToString();
        }

        public string GetActualScoreFromAllGamesPlayed()
        {
            return MoqDbAccess.Instance.CalculateTotalActualScore().ToString();
        }

        public string GetFormattedVowelsList()
        {
            var sb = new StringBuilder();

            foreach (var vowel in CurrentGame.VowelList)
            {
                sb.Append($"{vowel} ");
            }

            return sb.ToString();
        }

        public string GetFormattedConsonantsList()
        {
            var sb = new StringBuilder();

            foreach (var con in CurrentGame.ConsonantList)
            {
                sb.Append($"{con} ");
            }

            return sb.ToString();
        }

        public string GetLongestWordLength()
        {
            if (CurrentGame.LongestWordGuess.HasValue == false)
                return "0";
            else
                return CurrentGame.LongestWordGuess.ToString();
        }

        #endregion




    }
}
