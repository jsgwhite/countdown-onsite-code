﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using unirest_net.http;

namespace Countdown.BusinessLogic
{
    // I believe there are quite a few ways of calculating the longest possible word given a set of input chars.
    // However, for the purposes of this problem I'm not going to consider implementing my own search algo based on input values.
    // Instead I'm considering two options:
    //    1 - call a public API that returns the desired value for us
    //    2 - leave a simple placeholder value for now (this is a very simplified estimate rather than a real value).
    public class LongestWordCalculator
    {
        
        public static int GetLongestWordLengthFromPublicRestApi(List<char> vowelList, List<char> consonantList)
        {
            var allLettersJoined = JoinAllCharsIntoString(vowelList, consonantList);
            var bestWordLength = 0;
            var path = $"https://danielthepope-countdown-v1.p.rapidapi.com/solve/{allLettersJoined}?variance=0";


            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("X-RapidAPI-Host", "danielthepope-countdown-v1.p.rapidapi.com");
                client.DefaultRequestHeaders.Add("X-RapidAPI-Key", "3c96ccb57bmsha983a029e8aa861p1c8be0jsnd5ae63ea6cfa");

                HttpResponseMessage responseSync = client.GetAsync(path).Result;
                var read = responseSync.Content.ReadAsStringAsync().Result;
                var results = WordItem.CreateWordItemsFromJsonString(read);
                bestWordLength = results.Count > 0 ? results[0].length : 0;
            }

            return bestWordLength;
        }

        private static string JoinAllCharsIntoString(List<char> vowelList, List<char> consonantList)
        {
            return String.Join("", vowelList.Select(p => p.ToString()).ToArray()) +
                   String.Join("", consonantList.Select(p => p.ToString()).ToArray());
        }
    }


    public class WordItem
    {
        // The result from public API is a collection of WordItems,
        // so this method deserializes into a collection.
        public static List<WordItem> CreateWordItemsFromJsonString(string json)
        {
            using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(json)))
            {
                // Deserialization from JSON  
                DataContractJsonSerializer deserializer = new DataContractJsonSerializer(typeof(List<WordItem>));
                List<WordItem> words = (List<WordItem>) deserializer.ReadObject(ms);
                return words;
            }
        }


        public string word { get; set; }
        public int length { get; set; }
        public bool conundrum { get; set; }


    }
}
