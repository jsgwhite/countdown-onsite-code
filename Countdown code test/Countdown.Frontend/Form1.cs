﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Countdown.BusinessLogic;

namespace Countdown.Frontend
{
    public partial class Form1 : Form
    {
        private GameController ctlr = new GameController();

        public Form1()
        {
            InitializeComponent();
        }


        private void StartGameButton_Click(object sender, EventArgs e)
        {
            // Reset second section of form

            if (ctlr.CurrentGameRound > 0 && ctlr.CurrentGame.IsComplete == false)
            {
                statusMsgText.Text = "Unable to start new game until the previous game is completed.";
                return;
            }
            
            ctlr.CreateNewGame();
            currentGameText.Text = ctlr.CurrentGameRound.ToString();
            vowelsListText.Text = ctlr.GetFormattedVowelsList();
            consListText.Text = ctlr.GetFormattedConsonantsList();
            longestWordText.Text = ctlr.GetLongestWordLength();

            statusMsgText.Text = $"Game {ctlr.CurrentGameRound.ToString()} started.";
        }

        private void EndGameButton_Click(object sender, EventArgs e)
        {
            // Update first half of form

            // Check if allowed to end.
            // If allowed to end, then update the top half of form.

            if (ctlr.CurrentGame.IsAllowedToEndGame() == false)
            {
                statusMsgText.Text = "Unable to complete this game yet!";

            }
            else
            {
                statusMsgText.Text = "Game completed.";

                ctlr.EndGame();
                
                gamesPlayedCountText.Text = ctlr.CurrentGameRound.ToString();
                scoreText.Text = ctlr.GetActualScoreFromAllGamesPlayed();
                maxScoreText.Text = ctlr.GetBestPossibleScoreFromAllGamesPlayed();

                if (ctlr.IsGameComplete())
                {
                    // Todo: would be better to go thru and lock all controls here rather than just closing the form.
                    this.Close();
                }
            }
        }


        private void AddConButton_Click(object sender, EventArgs e)
        {
            // add conson

            if (ctlr.CurrentGame == null)
                return;
            if (ctlr.CurrentGame.VowelList.Count + ctlr.CurrentGame.ConsonantList.Count >= 9)
                return;

            ctlr.AddConsonantToCurrentGame();
            consListText.Text = ctlr.GetFormattedConsonantsList();
        }

        private void Button2_Click(object sender, EventArgs e)
        {

            if (ctlr.CurrentGame == null)
                return;
            if (ctlr.CurrentGame.VowelList.Count + ctlr.CurrentGame.ConsonantList.Count >= 9)
                return;

            ctlr.AddVowelToCurrentGame();
            vowelsListText.Text = ctlr.GetFormattedVowelsList();
        }
               
        private void NewWordButton_Click(object sender, EventArgs e)
        {
            // Not going to bother recording each word,
            // just enter in the max possible word length found.

            if (ctlr.CurrentGame == null)
                return;

            ctlr.CurrentGame.LongestWordGuess = int.Parse(longestWordText.Text);
        }
    }
}
