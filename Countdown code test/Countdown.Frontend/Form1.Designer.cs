﻿namespace Countdown.Frontend
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.gamesPlayedCountText = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.scoreText = new System.Windows.Forms.TextBox();
            this.maxScoreText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.currentGameText = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.vowelsListText = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.consListText = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.longestWordText = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.newWordButton = new System.Windows.Forms.Button();
            this.addVowelButton = new System.Windows.Forms.Button();
            this.addConButton = new System.Windows.Forms.Button();
            this.endGameButton = new System.Windows.Forms.Button();
            this.startGameButton = new System.Windows.Forms.Button();
            this.statusMsgText = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(116, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Countdown";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(126, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "No. of Games Played:";
            // 
            // gamesPlayedCountText
            // 
            this.gamesPlayedCountText.Enabled = false;
            this.gamesPlayedCountText.Location = new System.Drawing.Point(254, 78);
            this.gamesPlayedCountText.Name = "gamesPlayedCountText";
            this.gamesPlayedCountText.Size = new System.Drawing.Size(40, 20);
            this.gamesPlayedCountText.TabIndex = 2;
            this.gamesPlayedCountText.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(126, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Score:";
            // 
            // scoreText
            // 
            this.scoreText.Enabled = false;
            this.scoreText.Location = new System.Drawing.Point(254, 104);
            this.scoreText.Name = "scoreText";
            this.scoreText.Size = new System.Drawing.Size(40, 20);
            this.scoreText.TabIndex = 4;
            this.scoreText.Text = "0";
            // 
            // maxScoreText
            // 
            this.maxScoreText.Enabled = false;
            this.maxScoreText.Location = new System.Drawing.Point(254, 130);
            this.maxScoreText.Name = "maxScoreText";
            this.maxScoreText.Size = new System.Drawing.Size(40, 20);
            this.maxScoreText.TabIndex = 6;
            this.maxScoreText.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(126, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Max possible score:";
            // 
            // currentGameText
            // 
            this.currentGameText.Enabled = false;
            this.currentGameText.Location = new System.Drawing.Point(163, 213);
            this.currentGameText.Name = "currentGameText";
            this.currentGameText.Size = new System.Drawing.Size(131, 20);
            this.currentGameText.TabIndex = 8;
            this.currentGameText.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 220);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Current Game:";
            // 
            // vowelsListText
            // 
            this.vowelsListText.Enabled = false;
            this.vowelsListText.Location = new System.Drawing.Point(163, 241);
            this.vowelsListText.Name = "vowelsListText";
            this.vowelsListText.Size = new System.Drawing.Size(102, 20);
            this.vowelsListText.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 248);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Vowels:";
            // 
            // consListText
            // 
            this.consListText.Enabled = false;
            this.consListText.Location = new System.Drawing.Point(163, 267);
            this.consListText.Name = "consListText";
            this.consListText.Size = new System.Drawing.Size(102, 20);
            this.consListText.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 274);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "Consonants:";
            // 
            // longestWordText
            // 
            this.longestWordText.Location = new System.Drawing.Point(163, 293);
            this.longestWordText.Name = "longestWordText";
            this.longestWordText.Size = new System.Drawing.Size(102, 20);
            this.longestWordText.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 300);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 13);
            this.label8.TabIndex = 13;
            this.label8.Text = "Enter Longest Word:";
            // 
            // newWordButton
            // 
            this.newWordButton.Location = new System.Drawing.Point(272, 293);
            this.newWordButton.Name = "newWordButton";
            this.newWordButton.Size = new System.Drawing.Size(22, 23);
            this.newWordButton.TabIndex = 15;
            this.newWordButton.Text = "+";
            this.newWordButton.UseVisualStyleBackColor = true;
            this.newWordButton.Click += new System.EventHandler(this.NewWordButton_Click);
            // 
            // addVowelButton
            // 
            this.addVowelButton.Location = new System.Drawing.Point(272, 239);
            this.addVowelButton.Name = "addVowelButton";
            this.addVowelButton.Size = new System.Drawing.Size(22, 23);
            this.addVowelButton.TabIndex = 17;
            this.addVowelButton.Text = "+";
            this.addVowelButton.UseVisualStyleBackColor = true;
            this.addVowelButton.Click += new System.EventHandler(this.Button2_Click);
            // 
            // addConButton
            // 
            this.addConButton.Location = new System.Drawing.Point(272, 267);
            this.addConButton.Name = "addConButton";
            this.addConButton.Size = new System.Drawing.Size(22, 23);
            this.addConButton.TabIndex = 18;
            this.addConButton.Text = "+";
            this.addConButton.UseVisualStyleBackColor = true;
            this.addConButton.Click += new System.EventHandler(this.AddConButton_Click);
            // 
            // endGameButton
            // 
            this.endGameButton.Location = new System.Drawing.Point(205, 322);
            this.endGameButton.Name = "endGameButton";
            this.endGameButton.Size = new System.Drawing.Size(89, 23);
            this.endGameButton.TabIndex = 19;
            this.endGameButton.Text = "End Game";
            this.endGameButton.UseVisualStyleBackColor = true;
            this.endGameButton.Click += new System.EventHandler(this.EndGameButton_Click);
            // 
            // startGameButton
            // 
            this.startGameButton.Location = new System.Drawing.Point(205, 156);
            this.startGameButton.Name = "startGameButton";
            this.startGameButton.Size = new System.Drawing.Size(89, 23);
            this.startGameButton.TabIndex = 19;
            this.startGameButton.Text = "Start Game";
            this.startGameButton.UseVisualStyleBackColor = true;
            this.startGameButton.Click += new System.EventHandler(this.StartGameButton_Click);
            // 
            // statusMsgText
            // 
            this.statusMsgText.AutoSize = true;
            this.statusMsgText.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.statusMsgText.Location = new System.Drawing.Point(126, 366);
            this.statusMsgText.Name = "statusMsgText";
            this.statusMsgText.Size = new System.Drawing.Size(108, 13);
            this.statusMsgText.TabIndex = 20;
            this.statusMsgText.Text = "Game not started yet!";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 630);
            this.Controls.Add(this.statusMsgText);
            this.Controls.Add(this.startGameButton);
            this.Controls.Add(this.endGameButton);
            this.Controls.Add(this.addConButton);
            this.Controls.Add(this.addVowelButton);
            this.Controls.Add(this.newWordButton);
            this.Controls.Add(this.longestWordText);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.consListText);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.vowelsListText);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.currentGameText);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.maxScoreText);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.scoreText);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.gamesPlayedCountText);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox gamesPlayedCountText;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox scoreText;
        private System.Windows.Forms.TextBox maxScoreText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox currentGameText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox vowelsListText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox consListText;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox longestWordText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button newWordButton;
        private System.Windows.Forms.Button addVowelButton;
        private System.Windows.Forms.Button addConButton;
        private System.Windows.Forms.Button endGameButton;
        private System.Windows.Forms.Button startGameButton;
        private System.Windows.Forms.Label statusMsgText;
    }
}

