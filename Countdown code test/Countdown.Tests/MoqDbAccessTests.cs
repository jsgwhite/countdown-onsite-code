using System;
using System.Linq;
using Countdown.BusinessLogic;
using Countdown.DataAccess;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Countdown.Tests
{
    [TestClass]
    public class MoqDbAccessTests
    {
        [TestMethod]
        public void MoqDbAccess_CreateInstanceTest()
        {
            // Starts empty.
            Assert.AreEqual(0, MoqDbAccess.Instance.Games.Count);


            var model = MoqDbAccess.Instance.CreateNewGame();
            MoqDbAccess.Instance.EndGame(model, 10, 3);


            // Now not empty.
            Assert.AreEqual(0, MoqDbAccess.Instance.Games.Count);
            Assert.IsTrue(MoqDbAccess.Instance.Games.Contains(model));

        }
    }
}
