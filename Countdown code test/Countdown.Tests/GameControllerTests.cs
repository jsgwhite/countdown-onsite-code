using System;
using System.Collections.Generic;
using System.Linq;
using Countdown.BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Countdown.Tests
{
    [TestClass]
    public class GameControllerTests
    {
        List<char> vowels = new List<char> { 'a', 'e', 'i', 'o', 'u' };


        [TestMethod]
        public void StartGameTest()
        {
            var contr = new GameController();
            contr.CreateNewGame();

            Assert.IsNotNull(contr.CurrentGame);
        }

        [TestMethod]
        public void CreateConsonantTest()
        {
            var contr = new GameController();
            contr.CreateNewGame();

            var letter = contr.AddConsonantToCurrentGame();

            Assert.IsFalse(vowels.Contains(letter));
        }


        [TestMethod]
        public void CreateVowelTest()
        {
            var contr = new GameController();
            contr.CreateNewGame();

            var letter = contr.AddVowelToCurrentGame();

            Assert.IsTrue(vowels.Contains(letter));
        }
    }
}
