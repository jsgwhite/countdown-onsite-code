using System;
using System.Collections.Generic;
using System.Linq;
using Countdown.BusinessLogic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Countdown.Tests
{
    [TestClass]
    public class LongestWordCalculatorTests
    {
        
        [TestMethod]
        public void CallLongestWordCalculator_Basic()
        {
            var vowels = new List<char> {'a', 'i'};
            var cons = new List<char> { 'd', 'c' };

            //var length = LongestWordCalculator.GetLongestPossibleWordLength(vowels, cons);
            var length = LongestWordCalculator.GetLongestWordLengthFromPublicRestApi(vowels, cons);

            // I actually first thought was going to be 3, so nice surprise when actually came out with 4!
            Assert.IsTrue(length == 4);
        }

    }
}
