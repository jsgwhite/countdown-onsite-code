﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Countdown.DataAccess.Entities;

namespace Countdown.DataAccess
{
    // Don't really need this layer since we're not really using a DB as such.
    // However, this can act as a placeholder for the real DB accessor we'd want at some point.
    public class MoqDbAccess
    {
        #region Singleton

        private static MoqDbAccess _instance;

        public static MoqDbAccess Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new MoqDbAccess();
                return _instance;
            }
        }

        private MoqDbAccess()
        {
            Games = new List<GameModel>();
        }

        #endregion


        public List<GameModel> Games { get; set; }
        public List<int> BestPossibleScoreList { get; set; } = new List<int>();
        public List<int> ActualScoreList { get; set; } = new List<int>();


        public GameModel CreateNewGame()
        {
            var newGameId = GetCurrentMaxGameId();
            var newGame = new GameModel(++newGameId);

            return newGame;
        }


        public void EndGame(GameModel game, int bestScore, int actualScore)
        {
            // Imagine in real world this is going off to be stored in DB somewhere.

            BestPossibleScoreList.Add(bestScore);
            ActualScoreList.Add(actualScore);
            Games.Add(game);
        }


        public int CalculateTotalBestPossibleScore()
        {
            int total = 0;
            foreach (var score in BestPossibleScoreList)
                total += score;
            return total;
        }


        public int CalculateTotalActualScore()
        {
            int total = 0;
            foreach (var score in ActualScoreList)
                total += score;
            return total;
        }


        private int GetCurrentMaxGameId()
        {
            return Games.Count;
            //return Games.Max(g => g.GameId);
        }


    }
}
