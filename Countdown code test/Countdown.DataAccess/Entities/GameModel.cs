﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Countdown.DataAccess.Entities
{
    public class GameModel
    {
        public int GameId { get; set; }
        public int? LongestWordGuess { get; set; }
        public List<char> VowelList { get; set; }
        public List<char> ConsonantList { get; set; }
        public bool IsComplete { get; set; }
        public int Score { get; set; }
        //public int MaxPossibleScore { get; set; }


        public GameModel(int gameId)
        {
            GameId = gameId;
            VowelList = new List<char>();
            ConsonantList = new List<char>();
            LongestWordGuess = null;
        }


        public void EndGame()
        {
            IsComplete = true;
            
        }

        public bool IsAllowedToEndGame()
        {
            if (VowelList.Count + ConsonantList.Count < 9)
                return false;
            if (LongestWordGuess == null)
                return false;


            return true;
        }
    }

}
